clc
clear
x= iris_dataset;
size(x)
%creates self-organizing maps for classify samples with as as 
%much detailed as desired by selecting the number of neurons in each dimension of the layer.
net = selforgmap([8 8]);   
view(net)
[net,tr] = train(net,x);
nntraintool
y = net(x);
cluster_index = vec2ind(y);  %returns the index of the neuron with an output of 1, for each vector.
plotsomtop(net)  %plots the self-organizing maps topology of 64 neurons positioned in an 8x8 hexagonal grid
hold on
%plotsomhits(net,x)   %calculates the classes for each flower and shows the number of flowers in each class
hold on
%plotsomnc(net)  %shows the neuron neighbor connections
hold on
%plotsomnd(net)  %shows how distant (in terms of Euclidian distance) each neuron's class is from its neighbors
