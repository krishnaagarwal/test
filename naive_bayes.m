
clear all
clc
% Read from File
file = fopen('input.txt','r');
fS = '%d %s %s %s %s';
%[A, count] = fscanf(file,fS);
count = 0;
while ~feof(file)
    count = count+1;
    data{count,1} = fscanf(file,'%d',1);
    data{count,2} = fscanf(file,'%s',1);
    data{count,3} = fscanf(file,'%s',1);
    data{count,4} = fscanf(file,'%s',1);
    data{count,5} = fscanf(file,'%s',1);
    data{count,6} = fscanf(file,'%s',1);
end
fclose(file);
%Function calc calling
[outlook,temp,humidity,wind,play] = calc(data);

pos = play{2,2}/(play{2,2}+play{1,2});
neg = play{1,2}/(play{2,2}+play{1,2});
A = input('Enter Outlook ','s');
for i = 1 : 3
    if strcmp(outlook(i,1),A)
        pos = pos*outlook{i,2}/play{2,2};
        neg = neg*outlook{i,3}/play{1,2};
    end
end
B = input('Enter Temp ','s');
for i = 1 : 3
    if strcmp(temp(i,1),B)
        pos = pos*temp{i,2}/play{2,2};
        neg = neg*temp{i,3}/play{1,2};
    end
end
C = input('Enter Humidity ','s');
for i = 1 : 2
    if strcmp(humidity(i,1),C)
        pos = pos*humidity{i,2}/play{2,2};
        neg = neg*humidity{i,3}/play{1,2};
    end
end
D = input('Enter Wind ','s');
for i = 1 : 2
    if strcmp(wind(i,1),D)
        pos = pos*wind{i,2}/play{2,2};
        neg = neg*wind{i,3}/play{1,2};
    end
end
if pos > neg
    disp YES;
else
    disp No;
end

