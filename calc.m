

function [outlook,temp,humidity,wind,play] = calc(data)

  E = unique(data(:,2));
  for i = 1 : size(E)
      outlook(i,:) = {E{i} 0 0};
  end
  E = unique(data(:,3));
  for i = 1 : size(E)
      temp(i,:) = {E{i} 0 0};
  end
  E = unique(data(:,4));
  for i = 1 : size(E)
      humidity(i,:) = {E{i} 0 0};
  end
  E = unique(data(:,5));
  for i = 1 : size(E)
      wind(i,:) = {E{i} 0 0};
  end
  
  %disp(outlook);
  %outlook = {'Sunny' 0 0;'Overcast' 0 0; 'Rain' 0 0};
  %temp = {'Cool' 0 0;'Mild' 0 0;'Hot' 0 0};
  %humidity = {'High' 0 0;'Normal' 0 0};
  %wind = {'Strong' 0 0;'Weak' 0 0};
  
  play = {'No' 0;'Yes' 0};
  
  for i = 1 : size(data,1)
      for j = 1 : size(outlook,1)
          if strcmp(outlook(j,1),data(i,2))
              if strcmp('No',data(i,6)) == 1
                  outlook{j,3} = outlook{j,3}+1;
                  play{1,2} = play{1,2} + 1;
              else
                  outlook{j,2} = outlook{j,2}+1;
                  play{2,2} = play{2,2} + 1;
              end
          end
      end
      for j = 1 : size(temp,1)
          if strcmp(temp(j,1),data(i,3))
              if strcmp('No',data(i,6)) == 1
                  temp{j,3} = temp{j,3}+1;
              else
                  temp{j,2} = temp{j,2}+1;
              end
          end
      end
      for j = 1 : size(humidity,1)
          if strcmp(humidity(j,1),data(i,4))
              if strcmp('No',data(i,6)) == 1
                  humidity{j,3} = humidity{j,3}+1;
              else
                  humidity{j,2} = humidity{j,2}+1;
              end
          end
      end
      for j = 1 : size(wind,1)
          if strcmp(wind(j,1),data(i,5)) == 1
              if strcmp('No',data(i,6))
                  wind{j,3} = wind{j,3}+1;
              else
                  wind{j,2} = wind{j,2}+1;
              end
          end
      end
  end         
end
