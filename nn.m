clc
clear all
close all
load iris_dataset;
a=[1 1 1 1 1 1 0 0 0 1 1 1 1 1 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 0 0 0 1 0];
b=[1 1 1 1 1 0 0 0 0 1 0 0 0 0 1 1 1 1 1 1 0 1 0 0 0 0 0 0 1 0 0 0 0 0 1];
c=[1 1 1 1 1 0 0 0 0 1 0 0 0 0 1 1 1 1 1 1 1 1 0 0 1 1 1 1 1 0 1 0 0 0 0];
d=[1 0 0 0 1 0 1 0 1 0 0 0 1 0 0 0 0 0 0 0 1 0 0 0 1 0 0 0 0 0 1 1 1 1 1];
e=[1 0 0 0 0 1 0 0 0 0 1 0 0 0 1 1 1 1 1 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1];
f=[1 1 1 1 1 1 0 0 0 0 1 1 1 1 1 1 0 0 0 0 1 0 0 1 1 1 1 1 1 1 0 0 0 1 0];
g=[1 0 0 1 1 1 0 0 1 1 1 0 0 1 1 1 0 0 1 1 1 0 0 1 1 1 0 0 1 1 1 1 1 1 1];
h=[0 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 0 0 0 0 0 0 1 1 1 1 1];
i=[1 1 1 1 1 1 0 0 0 1 1 0 0 0 1 1 1 1 1 1 0 1 0 0 0 0 0 1 0 0 0 0 0 0 1];

input=[a;b;c;d;e;f;g;h;i];
target=[1;2;3;4;5;6;7;8;9]
net=feedforwardnet(5);
view(net);
net.divideParam.trainratio=70/100;
net.divideParam.valratio=15/100;
net.divideParam.testratio=15/100;
[net,tr]=train(net,input,target);
output=net(input);
error=gsubtract(output,target);
